package com.kotlin.firstapp.quiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.kotlin.firstapp.quiz.model_layer.Question
import com.kotlin.firstapp.quiz.view_models.QuizViewModel
import java.lang.Exception

private const val TAG = "MainActivity"
private const val KEY_INDEX = "index"

class MainActivity : AppCompatActivity() {

    private lateinit var trueButton: Button
    private lateinit var falseButton: Button
    private lateinit var nextButton: Button
    private lateinit var questionTextView: TextView

    private val quizViewModel: QuizViewModel by lazy {
        ViewModelProviders.of(this).get(QuizViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate(Bundle?) called")
        setContentView(R.layout.activity_main)

        val currentIndex = savedInstanceState?.getInt(KEY_INDEX, 0) ?: 0
        quizViewModel.currentIndex = currentIndex

        trueButton = findViewById(R.id.true_button)
        falseButton = findViewById(R.id.false_button)
        nextButton = findViewById(R.id.next_button)
        questionTextView = findViewById(R.id.question_text_view)

        shouldBeEnabled()

        trueButton.setOnClickListener {
            checkAnswer(true)
            shouldBeEnabled()
        }

        falseButton.setOnClickListener {
            checkAnswer(false)
            shouldBeEnabled()
        }

        nextButton.setOnClickListener {
            quizViewModel.moveToNext()
            val questionTextResId = quizViewModel.currentQuestionText
            questionTextView.setText(questionTextResId)
            shouldBeEnabled()
        }

        val questionTextResId = quizViewModel.currentQuestionText
        questionTextView.setText(questionTextResId)
    }

    private fun checkAnswer(answer: Boolean) {
        quizViewModel.questionBank[quizViewModel.currentIndex].isAnswered = true
        val toast: Toast = Toast.makeText(
            this, "",
            Toast.LENGTH_SHORT
        )
        toast.setGravity(Gravity.TOP, 0, 500)
        if (quizViewModel.currentQuestionAnswer == answer) {
            toast.setText(R.string.correct_toast)
            toast.show()
        } else {
            toast.setText(R.string.incorrect_toast)
            toast.show()
        }
    }

    private fun shouldBeEnabled() {
        if (quizViewModel.isCurrentQuestionIsAnswered) {
            falseButton.isEnabled = false
            trueButton.isEnabled = false
        } else {
            falseButton.isEnabled = true
            trueButton.isEnabled = true
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.i(TAG, "onSaveInstanceState")
        outState.putInt(KEY_INDEX, quizViewModel.currentIndex)
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")
    }

    override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }

    override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
    }
}